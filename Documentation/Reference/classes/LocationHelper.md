**CLASS**

# `LocationHelper`

```swift
public class LocationHelper: NSObject
```

## Properties
### `locationManager`

```swift
public var locationManager: CLLocationManager
```

### `currentLocation`

```swift
public private(set) var currentLocation: CLLocation?
```

### `currentLocations`

```swift
public private(set) var currentLocations: [CLLocation]?
```

### `lastKnowLocation`

```swift
public private(set) var lastKnowLocation: CLLocation?
```

### `locationChangedProtocol`

```swift
public var locationChangedProtocol: LocationChangedProtocol?
```

### `locationAuthorizationStatus`

```swift
public var locationAuthorizationStatus: CLAuthorizationStatus
```

### `hasLocationPermissions`

```swift
public var hasLocationPermissions: Bool
```

## Methods
### `setup(with:)`

```swift
public func setup(with accuracy: CLLocationAccuracy)
```

kCLLocationAccuracyBestForNavigation: CLLocationAccuracy
kCLLocationAccuracyBest: CLLocationAccuracy
kCLLocationAccuracyNearestTenMeters: CLLocationAccuracy
kCLLocationAccuracyHundredMeters: CLLocationAccuracy
kCLLocationAccuracyKilometer: CLLocationAccuracy
kCLLocationAccuracyThreeKilometers: CLLocationAccuracy
- Parameter accuracy: ex: kCLLocationAccuracyBestForNavigation

#### Parameters

| Name | Description |
| ---- | ----------- |
| accuracy | ex: kCLLocationAccuracyBestForNavigation |

### `setupAndStart(with:)`

```swift
public func setupAndStart(with accuracy: CLLocationAccuracy)
```

### `startUpdatingLocation()`

```swift
public func startUpdatingLocation()
```

### `stopUpdatingLocation()`

```swift
public func stopUpdatingLocation()
```
