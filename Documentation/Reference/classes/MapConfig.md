**CLASS**

# `MapConfig`

```swift
public class MapConfig: NSObject
```

## Properties
### `centerOnMe`

```swift
public var centerOnMe: Bool = false
```

### `zoomOutPois`

```swift
public var zoomOutPois: Bool = false
```

### `initialCoordinates`

```swift
public var initialCoordinates = CLLocationCoordinate2D(latitude: -9.1535, longitude: 38.7332)
```

### `updateLocationRate`

```swift
public var updateLocationRate = UserLocationUpdateEnum.never
```

### `routeColor`

```swift
public var routeColor = UIColor.green
```

### `routeLineWidth`

```swift
public var routeLineWidth: CGFloat = 4.0
```

### `isClusteringActive`

```swift
public var isClusteringActive: Bool = false
```

### `clusterTintColor`

```swift
public var clusterTintColor: UIColor = UIColor.systemOrange
```

## Methods
### `init()`

```swift
public override init()
```

### `init(centerOnMe:zoomOutPois:isClusteringActive:clusterTintColor:initialCoordinates:updateLocationRate:routeColor:routeLineWidth:)`

```swift
public convenience init(centerOnMe: Bool?, zoomOutPois: Bool?, isClusteringActive: Bool = false, clusterTintColor: UIColor = UIColor.systemOrange, initialCoordinates: CLLocationCoordinate2D?, updateLocationRate: UserLocationUpdateEnum?, routeColor: UIColor? = nil, routeLineWidth: CGFloat? = nil)
```
