**CLASS**

# `MapHelper`

```swift
public class MapHelper: NSObject
```

## Properties
### `mapView`

```swift
public private(set) var mapView: MapView
```

### `currentLocation`

```swift
public private(set) var currentLocation: CLLocation?
```

### `showUserLocation`

```swift
public var showUserLocation: Bool = false
```

### `containerView`

```swift
public var containerView: UIView?
```

### `locationChangedProtocol`

```swift
public var locationChangedProtocol: LocationChangedProtocol?
```

### `mapViewProtocol`

```swift
public var mapViewProtocol: MapViewProtocol?
```

### `mapManagerProtocol`

```swift
public var mapManagerProtocol: MapManagerProtocol?
```

### `mapType`

```swift
public var mapType: MKMapType
```

## Methods
### `init(view:mapconfig:showUserLocation:)`

```swift
public convenience init(view: UIView, mapconfig: MapConfig, showUserLocation: Bool)
```

### `init(view:mapconfig:mapViewProtocol:showUserLocation:)`

```swift
public init(view: UIView, mapconfig: MapConfig, mapViewProtocol: MapViewProtocol?, showUserLocation: Bool)
```

### `addMapToParentView()`

```swift
public func addMapToParentView()
```

### `setPointLocationDelegate(to:)`

```swift
public func setPointLocationDelegate(to delegate: PointLocationListenerProtocol)
```

### `centerMap(on:regionRadius:)`

```swift
public func centerMap(on location: CLLocation, regionRadius: Double)
```

Centers the map in the given location with the provided radius

### `centerMapOnUser(regionRadius:)`

```swift
public func centerMapOnUser(regionRadius: Double)
```

### `zoomMap(byFactor:)`

```swift
public func zoomMap(byFactor delta: Double)
```
