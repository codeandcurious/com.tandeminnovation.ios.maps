**CLASS**

# `MapView`

```swift
public class MapView: MKMapView
```

## Properties
### `mapViewProtocol`

```swift
public var mapViewProtocol: MapViewProtocol?
```

## Methods
### `touchesEnded(_:with:)`

```swift
public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
```
