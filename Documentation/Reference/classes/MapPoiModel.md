**CLASS**

# `MapPoiModel`

```swift
public class MapPoiModel: NSObject
```

## Properties
### `titleLabel`

```swift
public var titleLabel: String?
```

### `descriptionLabel`

```swift
public var descriptionLabel: String?
```

### `latitude`

```swift
public var latitude: Double?
```

### `longitude`

```swift
public var longitude: Double?
```

### `clLocationCoordinate2D`

```swift
public var clLocationCoordinate2D: CLLocationCoordinate2D?
```

### `clusterIdentifier`

```swift
public var clusterIdentifier: String?
```

The MapPoiModels with the same clusterIdentifier will be clustered

### `data`

```swift
public var data: Any?
```

## Methods
### `init()`

```swift
public override init()
```
