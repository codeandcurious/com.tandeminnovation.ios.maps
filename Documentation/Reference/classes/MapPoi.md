**CLASS**

# `MapPoi`

```swift
public class MapPoi<T>: Hashable, Equatable
```

## Properties
### `mkPointAnnotation`

```swift
public var mkPointAnnotation: MKPointAnnotation
```

### `mkAnnotationView`

```swift
public var mkAnnotationView: MKAnnotationView?
```

### `uuid`

```swift
public private(set) var uuid: UUID
```

## Methods
### `init(obj:mkPointAnnotation:)`

```swift
public init(obj: T, mkPointAnnotation: MKPointAnnotation)
```

### `==(_:_:)`

```swift
public static func == (lhs: MapPoi<T>, rhs: MapPoi<T>) -> Bool
```

#### Parameters

| Name | Description |
| ---- | ----------- |
| lhs | A value to compare. |
| rhs | Another value to compare. |

### `hash(into:)`

```swift
public func hash(into hasher: inout Hasher)
```

#### Parameters

| Name | Description |
| ---- | ----------- |
| hasher | The hasher to use when combining the components of this instance. |