**CLASS**

# `MapBoundsHelper`

```swift
public class MapBoundsHelper: NSObject
```

## Methods
### `getFullFrameBoundsCoordinates(from:)`

```swift
public func getFullFrameBoundsCoordinates(from mapView: MKMapView) -> [CLLocationCoordinate2D]
```

Calculates the NE NW SW SE coords of a given mapView.
- returns: Returns a array of CLLocationCoordinate2D with: [0] = neCoord ; [1] = nwCoord ; [2] = swCoord ; [3] = seCoord .

### `getCenterPoint(mapView:)`

```swift
public func getCenterPoint(mapView: MKMapView) -> CLLocationCoordinate2D
```

Calculates the center of a given mapView.
- returns: CLLocationCoordinate2D with the center of the given mapView
