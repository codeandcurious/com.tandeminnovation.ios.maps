**CLASS**

# `MapManager`

```swift
public class MapManager<T>: NSObject
```

## Properties
### `mapHelper`

```swift
public private(set) var mapHelper: MapHelper?
```

### `mapConfig`

```swift
public private(set) var mapConfig: MapConfig?
```

### `poiAdapter`

```swift
public private(set) var poiAdapter: PoiAdapter<T>?
```

### `behaviour`

```swift
public private(set) var behaviour: MarkerManagerBehaviour<T>?
```

### `mapContainer`

```swift
public var mapContainer: UIView?
```

### `locationPermissions`

```swift
public var locationPermissions: CLAuthorizationStatus?
```

## Methods
### `init(mapHelper:mapConfig:poiAdapter:behaviour:mapManagerProtocol:)`

```swift
public init(mapHelper: MapHelper, mapConfig: MapConfig, poiAdapter: PoiAdapter<T>, behaviour: MarkerManagerBehaviour<T>, mapManagerProtocol: MapManagerProtocol)
```

### `viewDidLoad()`

```swift
public func viewDidLoad()
```

### `viewDidUnload()`

```swift
public func viewDidUnload()
```

### `setCenterOnMe()`

```swift
public func setCenterOnMe()
```

Set the center on me , note that it just centers on me if zoomOutToSeePois is false.
If **zoomOutToSeePois == false** then the behaviour need to take "controll" of the zoom action
behaviour can be the default or custom

### `setCenter(on:)`

```swift
public func setCenter(on centerPoint: CLLocationCoordinate2D)
```

### `setPointLocationDelegate(to:)`

```swift
public func setPointLocationDelegate(to delegate: PointLocationListenerProtocol)
```

### `showMap()`

```swift
public func showMap()
```

### `setMapPois(_:_:)`

```swift
@discardableResult public func setMapPois(_ poiModelArray: [T]?, _ unwrapPinBlock: (T) -> MKPointAnnotation) -> [MapPoi<T>]?
```

Build and set MKPointAnnotations from a poiModelArray<T>
  - Parameters:
  - poiModelArray: The poiModel Array
  - unwrapPinBlock: Closure to unwrap the poiModel to MKPointAnnotation

### `addMapPois(_:_:)`

```swift
@discardableResult public func addMapPois(_ poiModelArray: [T]?, _ unwrapPinBlock: (T) -> MKPointAnnotation) -> [MapPoi<T>]?
```

Build and add MKPointAnnotations from a poiModelArray<T>
  - Parameters:
  - poiModelArray: The poiModel Array
  - unwrapPinBlock: Closure to unwrap the poiModel to MKPointAnnotation

### `removeMapPois(_:)`

```swift
public func removeMapPois(_ mapPois: MapPoi<T>...)
```

Removes from the current MapPoi array the given array and from the Map annotations
- Parameter mapPois: mapPois varArgs

#### Parameters

| Name | Description |
| ---- | ----------- |
| mapPois | mapPois varArgs |

### `removeMapPois(_:)`

```swift
public func removeMapPois(_ mapPois: [MapPoi<T>])
```

Removes from the current MapPoi array the given array and from the Map annotations
- Parameter mapPois: mapPois array

#### Parameters

| Name | Description |
| ---- | ----------- |
| mapPois | mapPois array |

### `removeAllMKAnnotations()`

```swift
public func removeAllMKAnnotations()
```

Remove all MKAnnotations in mapView

### `removeMKAnnotations(_:)`

```swift
public func removeMKAnnotations(_ annotations: MKAnnotation...)
```

Remove parameter annotations in mapView
- Parameter annotations: annotations: [MKAnnotation]

#### Parameters

| Name | Description |
| ---- | ----------- |
| annotations | annotations: [MKAnnotation] |

### `removeMKAnnotations(_:)`

```swift
public func removeMKAnnotations(_ annotations: [MKAnnotation])
```

Remove parameter annotations in mapView
- Parameter annotations: annotations: [MKAnnotation]

#### Parameters

| Name | Description |
| ---- | ----------- |
| annotations | annotations: [MKAnnotation] |

### `addAutomobileRoute(_:_:)`

```swift
public func addAutomobileRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation)
```

addAutomobileRoute
- Parameters:
  - sourceLocation: MapPointAnnotation
  - destinationLocation: MapPointAnnotation

#### Parameters

| Name | Description |
| ---- | ----------- |
| sourceLocation | MapPointAnnotation |
| destinationLocation | MapPointAnnotation |

### `addWalkingRoute(_:_:)`

```swift
public func addWalkingRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation)
```

addWalkingRoute
- Parameters:
  - sourceLocation: MapPointAnnotation
  - destinationLocation: MapPointAnnotation

#### Parameters

| Name | Description |
| ---- | ----------- |
| sourceLocation | MapPointAnnotation |
| destinationLocation | MapPointAnnotation |

### `addRoute(_:_:_:)`

```swift
public func addRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation, _ transportionType: MKDirectionsTransportType)
```

addRoute
- Parameters:
  - sourceLocation: MapPointAnnotation
  - destinationLocation: MapPointAnnotation

#### Parameters

| Name | Description |
| ---- | ----------- |
| sourceLocation | MapPointAnnotation |
| destinationLocation | MapPointAnnotation |

### `addMkOverlays(_:)`

```swift
public func addMkOverlays(_ mkOverlays: MKOverlay...)
```

Variadic func to add any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
- Parameter mkOverlays: MKOverlay...

#### Parameters

| Name | Description |
| ---- | ----------- |
| mkOverlays | MKOverlay… |

### `addMkOverlays(_:)`

```swift
public func addMkOverlays(_ mkOverlays: [MKOverlay])
```

Variadic func to add any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
Needed because swift doesn't convert variadic func to array as parameters
- Parameter mkOverlays: [MKOverlay]

#### Parameters

| Name | Description |
| ---- | ----------- |
| mkOverlays | [MKOverlay] |

### `removeAllMkOverlays()`

```swift
public func removeAllMkOverlays()
```

Remove all MkOverlays in the mkMapView

### `removeMkOverlays(_:)`

```swift
public func removeMkOverlays(_ mkOverlays: MKOverlay...)
```

Variadic func to remove any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
- Parameter mkOverlays: MKOverlay...

#### Parameters

| Name | Description |
| ---- | ----------- |
| mkOverlays | MKOverlay… |

### `removeMkOverlays(_:)`

```swift
public func removeMkOverlays(_ mkOverlays: [MKOverlay])
```

Variadic func to remove any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
Needed because swift doesn't convert variadic func to array as parameters
- Parameter mkOverlays: [MKOverlay]

#### Parameters

| Name | Description |
| ---- | ----------- |
| mkOverlays | [MKOverlay] |