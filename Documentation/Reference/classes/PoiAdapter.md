**CLASS**

# `PoiAdapter`

```swift
public class PoiAdapter<T>: NSObject
```

## Properties
### `mapPois`

```swift
public private(set) var mapPois = [MapPoi<T>]()
```

Array of MapPoi

## Methods
### `setPois(poiModelArray:closure:)`

```swift
public func setPois(poiModelArray: [T], closure: (T) -> MKPointAnnotation)
```

Sets a new MapPoi array with the given array
- Parameters:
  - poiModelArray: The Array of objects
  - closure: Closure where the transform occurs

#### Parameters

| Name | Description |
| ---- | ----------- |
| poiModelArray | The Array of objects |
| closure | Closure where the transform occurs |

### `addPois(poiModelArray:closure:)`

```swift
public func addPois(poiModelArray: [T], closure: (T) -> MKPointAnnotation)
```

This function will add MapPois to the current list/array
- Parameters:
  - poiModelArray: The Array of objects
  - closure: Closure where the transform occurs

#### Parameters

| Name | Description |
| ---- | ----------- |
| poiModelArray | The Array of objects |
| closure | Closure where the transform occurs |

### `removeMapPois(_:)`

```swift
public func removeMapPois(_ mapPoiList: [MapPoi<T>])
```

Removes from the current MapPoi array the given array
- Parameter mapPoiList: mapPoiList to remove

#### Parameters

| Name | Description |
| ---- | ----------- |
| mapPoiList | mapPoiList to remove |

### `removeAllMapPois()`

```swift
public func removeAllMapPois()
```

Removes all MapPois

### `sortPoisByDistance(location:)`

```swift
public func sortPoisByDistance(location: CLLocation)
```

Sort the MapPoi array by the nearest location to the given location
- Parameter location: Location to compare

#### Parameters

| Name | Description |
| ---- | ----------- |
| location | Location to compare |