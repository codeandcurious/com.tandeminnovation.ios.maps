**CLASS**

# `MarkerManagerBehaviour`

```swift
public class MarkerManagerBehaviour<T>: MapBehaviourBase
```

## Methods
### `init(mapHelper:adapter:mapConfig:)`

```swift
public init(mapHelper: MapHelper, adapter: PoiAdapter<T>, mapConfig: MapConfig)
```

### `populateMap(poiModelArray:)`

```swift
public func populateMap(poiModelArray: [MapPoi<T>])
```

### `zoomOutToNearestPoi()`

```swift
public func zoomOutToNearestPoi()
```

Function that show the nearest poi
The center of the map is the current user location
This function sorts the Poi array

### `zoomOutToSeePoi(poi:)`

```swift
public func zoomOutToSeePoi(poi: MapPoi<T>)
```

Function that zooms out to see a given poi in the map
The center of the map is the current user location

### `zoomOutToDefaultPoi()`

```swift
public func zoomOutToDefaultPoi()
```
