**CLASS**

# `MapPointAnnotation`

```swift
public class MapPointAnnotation: MKPointAnnotation
```

## Properties
### `selectedImage`

```swift
public var selectedImage: UIImage? = nil
```

### `unselectedImage`

```swift
public var unselectedImage: UIImage? = nil
```

### `activeImage`

```swift
public var activeImage: UIImage? = nil
```

### `inactiveImage`

```swift
public var inactiveImage: UIImage? = nil
```

### `visitedImage`

```swift
public var visitedImage: UIImage? = nil
```

### `isSelected`

```swift
public var isSelected: Bool = false
```

### `isActive`

```swift
public var isActive: Bool = true
```

### `isVisited`

```swift
public var isVisited: Bool = false
```

### `pinAnnotationView`

```swift
public var pinAnnotationView: MKPinAnnotationView? = nil
```

### `poiModel`

```swift
public var poiModel: MapPoiModel? = nil
```

### `pinImage`

```swift
public var pinImage: UIImage?
```

state map:

- SELECTED | T | F |(T)| F |
- ACTIVE   |(T)| T | T |(T)|
- VISITED  | T |(T)| F | F |

To work you need to set all images
