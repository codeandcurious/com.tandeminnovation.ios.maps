**PROTOCOL**

# `PointLocationListenerProtocol`

```swift
public protocol PointLocationListenerProtocol
```

A delegate for the MapManagerListener will receive the Coordinates of a point in a view, translated to the mapView.

The view should have the same area as the map.

## Methods
### `didReceive(mapCoordinates:screenCoordinates:forView:)`

```swift
func didReceive(mapCoordinates: CLLocationCoordinate2D, screenCoordinates: CGPoint, forView view: UIView?)
```

Delegate method that will handle the measured coordinate

- Parameters:
  - mapCoordinates: The measured coordinates sent by the MapManager
  - screenCoordinates: The Point measured in willSendPoint and sent to the MapManager
  - view: the view used in willSendView and sent to the MapManager

#### Parameters

| Name | Description |
| ---- | ----------- |
| mapCoordinates | The measured coordinates sent by the MapManager |
| screenCoordinates | The Point measured in willSendPoint and sent to the MapManager |
| view | the view used in willSendView and sent to the MapManager |

### `willSendPoint()`

```swift
func willSendPoint() -> CGPoint?
```

The point in the are of the view

- Returns: Sends the point to the manager

### `willSendView()`

```swift
func willSendView() -> UIView?
```

The view that measured the the point.

- Returns: Sends the view that measured the point
