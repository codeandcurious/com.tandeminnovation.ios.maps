**PROTOCOL**

# `LocationChangedProtocol`

```swift
public protocol LocationChangedProtocol
```

## Methods
### `onLocationsUpdated(_:)`

```swift
func onLocationsUpdated(_ locations: [CLLocation])
```

### `onLocationError(error:)`

```swift
func onLocationError(error: Error)
```
