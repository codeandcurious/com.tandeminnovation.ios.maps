**PROTOCOL**

# `MapManagerProtocol`

```swift
public protocol MapManagerProtocol
```

## Methods
### `mapWillAppear()`

```swift
func mapWillAppear()
```

### `mapDidAppear()`

```swift
func mapDidAppear()
```

### `mapDidMove()`

```swift
func mapDidMove()
```

### `mapWillMove()`

```swift
func mapWillMove()
```
