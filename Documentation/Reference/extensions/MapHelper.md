**EXTENSION**

# `MapHelper`
```swift
extension MapHelper: MKMapViewDelegate
```

## Methods
### `mapView(_:didUpdate:)`

```swift
public func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation)
```

### `mapView(_:viewFor:)`

```swift
public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
```

### `mapView(_:didSelect:)`

```swift
public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
```

### `mapView(_:didDeselect:)`

```swift
public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView)
```

### `mapView(_:rendererFor:)`

```swift
public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
```

### `mapView(_:regionWillChangeAnimated:)`

```swift
public func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool)
```

### `mapView(_:regionDidChangeAnimated:)`

```swift
public func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
```
