**EXTENSION**

# `MapManager`
```swift
extension MapManager: LocationChangedProtocol
```

## Methods
### `onLocationsUpdated(_:)`

```swift
public func onLocationsUpdated(_ locations: [CLLocation])
```

### `onLocationError(error:)`

```swift
public func onLocationError(error: Error)
```
