**EXTENSION**

# `MapManagerProtocol`
```swift
public extension MapManagerProtocol
```

## Methods
### `mapWillAppear()`

```swift
func mapWillAppear()
```

### `mapDidAppear()`

```swift
func mapDidAppear()
```

### `mapDidMove()`

```swift
func mapDidMove()
```

### `mapWillMove()`

```swift
func mapWillMove()
```
