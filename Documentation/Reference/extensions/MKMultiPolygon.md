**EXTENSION**

# `MKMultiPolygon`
```swift
extension MKMultiPolygon: NSCopying
```

## Properties
### `id`

```swift
public var id: Any
```

### `lineWidth`

```swift
public var lineWidth: CGFloat
```

### `strokeColor`

```swift
public var strokeColor: UIColor
```

### `fillColor`

```swift
public var fillColor: UIColor
```

## Methods
### `contains(_:)`

```swift
public func contains(_ coordinate2D: CLLocationCoordinate2D) -> Bool
```

### `copy(with:)`

```swift
public func copy(with zone: NSZone? = nil) -> Any
```
