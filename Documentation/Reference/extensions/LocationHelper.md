**EXTENSION**

# `LocationHelper`
```swift
extension LocationHelper: CLLocationManagerDelegate
```

## Methods
### `locationManager(manager:didUpdateToLocation:fromLocation:)`

```swift
public func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation)
```

### `locationManager(_:didUpdateLocations:)`

```swift
public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
```

### `locationManager(_:didFailWithError:)`

```swift
public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
```
