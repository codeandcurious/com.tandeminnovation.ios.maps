**EXTENSION**

# `MapViewProtocol`
```swift
public extension MapViewProtocol
```

## Methods
### `didTapMap(_:_:)`

```swift
func didTapMap(_ mapView: MKMapView, _ clLocationCoordinate2D: CLLocationCoordinate2D)
```

### `didTapMKPolygons(_:_:)`

```swift
func didTapMKPolygons(_ mapView: MKMapView, _ mkPolygons: [MKPolygon])
```

### `didTapMKCircles(_:_:)`

```swift
func didTapMKCircles(_ mapView: MKMapView, _ mkCircles: [MKCircle])
```

### `didTapMKPolylines(_:_:)`

```swift
func didTapMKPolylines(_ mapView: MKMapView, _ mkPolylines: [MKPolyline])
```

### `didTapMKMultiPolygons(_:_:)`

```swift
func didTapMKMultiPolygons(_ mapView: MKMapView, _ mkMultiPolygons: [MKMultiPolygon])
```

### `didTapMKMultiPolylines(_:_:)`

```swift
func didTapMKMultiPolylines(_ mapView: MKMapView, _ mkMultiPolylines: [MKMultiPolyline])
```

### `didSelectMKAnnotationView(_:mapPointAnnotation:mkAnnotationView:)`

```swift
func didSelectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView)
```

### `didDeselectMKAnnotationView(_:mapPointAnnotation:mkAnnotationView:)`

```swift
func didDeselectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView)
```
