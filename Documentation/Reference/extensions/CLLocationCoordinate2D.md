**EXTENSION**

# `CLLocationCoordinate2D`
```swift
extension CLLocationCoordinate2D: Equatable
```

## Methods
### `distance(from:)`

```swift
public func distance(from: CLLocationCoordinate2D) -> CLLocationDistance
```

### `addMeters(bearing:distanceMeters:)`

```swift
public func addMeters(bearing: Double, distanceMeters: Double) -> CLLocationCoordinate2D
```

Description
- Parameters:
  - bearing: north: bearing = 0, for east: bearing = 90, for south: bearing = 180, for west: bearing = 270
  - distanceMeters: distanceMeters description
- Returns: CLLocationCoordinate2D

#### Parameters

| Name | Description |
| ---- | ----------- |
| bearing | north: bearing = 0, for east: bearing = 90, for south: bearing = 180, for west: bearing = 270 |
| distanceMeters | distanceMeters description |

### `==(_:_:)`

```swift
public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool
```

#### Parameters

| Name | Description |
| ---- | ----------- |
| lhs | A value to compare. |
| rhs | Another value to compare. |