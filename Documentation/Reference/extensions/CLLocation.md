**EXTENSION**

# `CLLocation`
```swift
extension CLLocation
```

## Methods
### `reverseGeocodeLocationAsync(completion:)`

```swift
public func reverseGeocodeLocationAsync(completion: @escaping (_ placeMark: CLPlacemark?, _ error: Error?) -> ())
```

This method submits a CLLocation data to the geocoding server asynchronously and returns a CLPlacemark with information about the location (ex: CountryCode, City, Address etc etc).
When the request completes, the geocoder executes the provided completion handler on the main thread.
- Parameter completion: A completion handler with CLPlacemark? and Error? ex: (placeMark,  error) in

#### Parameters

| Name | Description |
| ---- | ----------- |
| completion | A completion handler with CLPlacemark? and Error? ex: (placeMark,  error) in |