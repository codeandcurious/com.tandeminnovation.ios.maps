**ENUM**

# `MapViewInteractions`

```swift
public enum MapViewInteractions: Int
```

## Cases
### `mkPolygonInteraction`

```swift
case mkPolygonInteraction = 1
```

### `mkMultiPolygonInteraction`

```swift
case mkMultiPolygonInteraction = 2
```

### `mkAnnotation`

```swift
case mkAnnotation = 3
```
