**ENUM**

# `UserLocationUpdateEnum`

```swift
public enum UserLocationUpdateEnum: Int
```

## Cases
### `once`

```swift
case once = 1
```

### `continuous`

```swift
case continuous = 2
```

### `never`

```swift
case never = 3
```
