![tandem-innovation](tandem-innovation.png)

## Maps

Maps it's a MapKit helper and wrapper. The main goal is to integrate MapKit faster and simpler within a project


## Installation

#### [CocoaPods](http://cocoapods.org)

````ruby
use_frameworks!

pod 'Maps', :git => 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.maps.git', :branch => 'master', :tag => 'LATEST_TAG'
````

#### [Carthage](https://github.com/Carthage/Carthage)

````bash
git "https://firetrap@bitbucket.org/codeandcurious/com.tandeminnovation.ios.maps.git" == LATEST_TAG
````

#### [Swift Package Manager](https://swift.org/package-manager/)

```swift
dependencies: [
    .package(url: "https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.maps.git", from: "LATEST_TAG")
]
```
## License

Maps is available under the MIT license. See the * [LICENSE](LICENSE) file for more info.
