Pod::Spec.new do |s|
  s.name             = 'Maps'
  s.version          = '1.5.2'
  s.summary          = 'Tandem innovation Maps library'
  s.description      = <<-DESC
MapKit Wrapper and helper to deploy fast and reliable MapKit Integrations.
DESC

  s.homepage         = 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.maps/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'firetrap' => 'fabio.barreiros@nextreality.pt' }
  s.source           = { :git => 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.maps.git', :tag => s.version.to_s }
  s.swift_version = '5'
  s.ios.deployment_target = '13.0'
  s.source_files = 'Sources/**/*{swift,h,m}'

end
