/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit
import MapKit

public class MapConfig: NSObject {

	public var centerOnMe: Bool = false
	public var zoomOutPois: Bool = false
	public var initialCoordinates = CLLocationCoordinate2D(latitude: -9.1535, longitude: 38.7332) //Lisbon coordinates as default
	public var updateLocationRate = UserLocationUpdateEnum.never
	public var routeColor = UIColor.green
	public var routeLineWidth: CGFloat = 4.0
	public var isClusteringActive: Bool = false
	public var clusterTintColor: UIColor = UIColor.systemOrange
	public override init() {
		super.init()
	}

	public convenience init(centerOnMe: Bool?, zoomOutPois: Bool?, isClusteringActive: Bool = false, clusterTintColor: UIColor = UIColor.systemOrange, initialCoordinates: CLLocationCoordinate2D?, updateLocationRate: UserLocationUpdateEnum?, routeColor: UIColor? = nil, routeLineWidth: CGFloat? = nil) {
		self.init()

		self.isClusteringActive = isClusteringActive
		self.clusterTintColor = clusterTintColor

		if let centerOnMe = centerOnMe {

			self.centerOnMe = centerOnMe
		}
		if let zoomOutPois = zoomOutPois {

			self.zoomOutPois = zoomOutPois
		}
		if let initialCoordinates = initialCoordinates {

			self.initialCoordinates = initialCoordinates
		}
		if let updateLocationRate = updateLocationRate {

			self.updateLocationRate = updateLocationRate
		}
		if let routeColor = routeColor {

			self.routeColor = routeColor
		}
		if let routeLineWidth = routeLineWidth {

			self.routeLineWidth = routeLineWidth
		}
	}
}
