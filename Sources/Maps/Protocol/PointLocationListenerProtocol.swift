/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import CoreLocation


/// A delegate for the MapManagerListener will receive the Coordinates of a point in a view, translated to the mapView.
///
/// The view should have the same area as the map.
public protocol PointLocationListenerProtocol {

	/// Delegate method that will handle the measured coordinate
	///
	/// - Parameters:
	///   - mapCoordinates: The measured coordinates sent by the MapManager
	///   - screenCoordinates: The Point measured in willSendPoint and sent to the MapManager
	///   - view: the view used in willSendView and sent to the MapManager
	func didReceive(mapCoordinates: CLLocationCoordinate2D, screenCoordinates: CGPoint, forView view: UIView?)


	/// The point in the are of the view
	///
	/// - Returns: Sends the point to the manager
	func willSendPoint() -> CGPoint?

	/// The view that measured the the point.
	///
	/// - Returns: Sends the view that measured the point
	func willSendView() -> UIView?
}
