/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit

public protocol MapViewProtocol {

    func didTapMKPolygons(_ mapView: MKMapView, _ mkPolygons: [MKPolygon])

    func didTapMKCircles(_ mapView: MKMapView, _ mkCircles: [MKCircle])

    func didTapMKPolylines(_ mapView: MKMapView, _ mkPolylines: [MKPolyline])

    @available(iOS 13.0, *)
    func didTapMKMultiPolygons(_ mapView: MKMapView, _ mkMultiPolygons: [MKMultiPolygon])

    @available(iOS 13.0, *)
    func didTapMKMultiPolylines(_ mapView: MKMapView, _ mkMultiPolylines: [MKMultiPolyline])

    func didTapMap(_ mapView: MKMapView, _ clLocationCoordinate2D: CLLocationCoordinate2D)

    func didSelectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView)

    func didDeselectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView)
}
public extension MapViewProtocol {

    func didTapMap(_ mapView: MKMapView, _ clLocationCoordinate2D: CLLocationCoordinate2D) { }
}
public extension MapViewProtocol {

    func didTapMKPolygons(_ mapView: MKMapView, _ mkPolygons: [MKPolygon]) { }

}
public extension MapViewProtocol {

    func didTapMKCircles(_ mapView: MKMapView, _ mkCircles: [MKCircle]) { }

}
public extension MapViewProtocol {

    func didTapMKPolylines(_ mapView: MKMapView, _ mkPolylines: [MKPolyline]) { }

}
public extension MapViewProtocol {

    @available(iOS 13.0, *)
    func didTapMKMultiPolygons(_ mapView: MKMapView, _ mkMultiPolygons: [MKMultiPolygon]) { }

}
public extension MapViewProtocol {

    @available(iOS 13.0, *)
    func didTapMKMultiPolylines(_ mapView: MKMapView, _ mkMultiPolylines: [MKMultiPolyline]) { }

}
public extension MapViewProtocol {

    func didSelectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView) { }

}
public extension MapViewProtocol {

    func didDeselectMKAnnotationView(_ mapView: MKMapView, mapPointAnnotation: MapPointAnnotation?, mkAnnotationView: MKAnnotationView) { }
}
