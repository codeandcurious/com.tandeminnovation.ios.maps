/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit
import MapKit
import CoreLocation

public class PoiAdapter<T>: NSObject {

    /// Array of MapPoi
    public private(set) var mapPois = [MapPoi<T>]()

    /// Sets a new MapPoi array with the given array
    /// - Parameters:
    ///   - poiModelArray: The Array of objects
    ///   - closure: Closure where the transform occurs
    public func setPois(poiModelArray: [T], closure: (T) -> MKPointAnnotation) {
        self.mapPois = [MapPoi<T>]() //Clear the array
        self.addPois(poiModelArray: poiModelArray, closure: closure)
    }

    /// This function will add MapPois to the current list/array
    /// - Parameters:
    ///   - poiModelArray: The Array of objects
    ///   - closure: Closure where the transform occurs
    public func addPois(poiModelArray: [T], closure: (T) -> MKPointAnnotation) {
        for poiModel in poiModelArray {

            let poi = MapPoi(obj: poiModel, mkPointAnnotation: closure(poiModel))
            self.mapPois.append(poi)
        }
    }

    /// Removes from the current MapPoi array the given array
    /// - Parameter mapPoiList: mapPoiList to remove
    public func removeMapPois(_ mapPoiList: [MapPoi<T>]) {

        self.mapPois = Array(Set(mapPois).subtracting(Set(mapPoiList))) //Subtract from mapPois the mapPoisList received in param
    }

    /// Removes all MapPois
    public func removeAllMapPois() {

        self.mapPois.removeAll()
    }

    /// Sort the MapPoi array by the nearest location to the given location
    /// - Parameter location: Location to compare
    public func sortPoisByDistance(location: CLLocation) {
        self.mapPois.sort { (firstPoi: MapPoi<T>, secondPoi: MapPoi<T>) -> Bool in
            let dist1: CLLocationDistance = self.getNearestLocation(poi: firstPoi, location: location)
            let dist2: CLLocationDistance = self.getNearestLocation(poi: secondPoi, location: location)

            return dist1 < dist2
        }
    }

    private func getNearestLocation(poi: MapPoi<T>, location: CLLocation) -> CLLocationDistance {
        let loc = CLLocation(latitude: poi.mkPointAnnotation.coordinate.latitude, longitude: poi.mkPointAnnotation.coordinate.longitude)
        let dist = loc.distance(from: location)

        return dist
    }
}
