/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
import Foundation
import MapKit

// TODO: refactor zoomOutToNearestPoi, zoomOutToSeePoi(poi:), zoomOutToDefaultPoi()
public class MarkerManagerBehaviour<T>: MapBehaviourBase {

	private var mapHelper: MapHelper
	private var mapAdapter: PoiAdapter<T>
	private var mapConfig: MapConfig
	private var sortedMapPois = [Int: CLLocationDistance]()
	private let REGION_MARGIN: Double = 50 // magic number 50 : to draw a region to include the poi , and to see better the poi i added a 50 margin

	public init(mapHelper: MapHelper, adapter: PoiAdapter<T>, mapConfig: MapConfig) {
		self.mapHelper = mapHelper
		self.mapAdapter = adapter
		self.mapConfig = mapConfig
	}

	public func populateMap(poiModelArray: [MapPoi<T>]) {

		for poi in poiModelArray {

			self.mapHelper.mapView.addAnnotation(poi.mkPointAnnotation)
		}
	}

	/**
     Function that show the nearest poi
     The center of the map is the current user location
     This function sorts the Poi array
     */
	public func zoomOutToNearestPoi() {
		let userLocation: CLLocation = self.mapHelper.currentLocation! // FIXME: what if nil?
		self.mapAdapter.sortPoisByDistance(location: userLocation)
		let nearestPoi = self.mapAdapter.mapPois[0]
		let distance = self.mapHelper.mapView.userLocation.location?.distance(from: CLLocation(latitude: nearestPoi.mkPointAnnotation.coordinate.latitude, longitude: nearestPoi.mkPointAnnotation.coordinate.longitude))
		let region = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: distance! * 2 + self.REGION_MARGIN, longitudinalMeters: distance! * 2 + self.REGION_MARGIN)
		self.mapHelper.mapView.setRegion(region, animated: true)
	}

	/**
     Function that zooms out to see a given poi in the map
     The center of the map is the current user location
     */
	public func zoomOutToSeePoi(poi: MapPoi<T>) {
		let userLocation: CLLocation = self.mapHelper.currentLocation! // FIXME: what if nil?
		let distance = self.mapHelper.mapView.userLocation.location?.distance(from: CLLocation(latitude: poi.mkPointAnnotation.coordinate.latitude, longitude: poi.mkPointAnnotation.coordinate.longitude))
		let region = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: distance! * 2 + self.REGION_MARGIN, longitudinalMeters: distance! * 2 + self.REGION_MARGIN)
		self.mapHelper.mapView.setRegion(region, animated: true)
	}

	public func zoomOutToDefaultPoi() {
		self.mapAdapter.sortPoisByDistance(location: CLLocation(latitude: mapConfig.initialCoordinates.latitude, longitude: mapConfig.initialCoordinates.longitude))
		let nearestPoi = self.mapAdapter.mapPois[0]
		let distance = self.mapHelper.mapView.userLocation.location?.distance(from: CLLocation(latitude: nearestPoi.mkPointAnnotation.coordinate.latitude, longitude: nearestPoi.mkPointAnnotation.coordinate.longitude))
		let region = MKCoordinateRegion(center: mapConfig.initialCoordinates, latitudinalMeters: distance! * 2 + self.REGION_MARGIN, longitudinalMeters: distance! * 2 + self.REGION_MARGIN)
		self.mapHelper.mapView.setRegion(region, animated: true)
	}
}
