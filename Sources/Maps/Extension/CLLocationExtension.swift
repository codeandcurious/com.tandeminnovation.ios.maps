/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit

extension CLLocation {
    
    /// This method submits a CLLocation data to the geocoding server asynchronously and returns a CLPlacemark with information about the location (ex: CountryCode, City, Address etc etc).
    /// When the request completes, the geocoder executes the provided completion handler on the main thread.
    /// - Parameter completion: A completion handler with CLPlacemark? and Error? ex: (placeMark,  error) in
    public func reverseGeocodeLocationAsync(completion: @escaping (_ placeMark: CLPlacemark?, _ error: Error?) -> ()) {

        CLGeocoder().reverseGeocodeLocation(self) { (placeMark, error) in

            completion(placeMark?.first, error)
        }
    }
}
