/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit

@available(iOS 13.0, *)
extension MKMultiPolygon: NSCopying {

    private static var lineWidth = ExtensionProperty<CGFloat>()
    private static var strokeColor = ExtensionProperty<UIColor>()
    private static var fillColor = ExtensionProperty<UIColor>()
    private static var id = ExtensionProperty<Any>()

    public var id: Any {
        get {
            return MKMultiPolygon.id.get(self) ?? UUID().uuidString
        }
        set(newValue) {
            MKMultiPolygon.id.set(self, value: newValue)
        }
    }

    public var lineWidth: CGFloat {
        get {
            return MKMultiPolygon.lineWidth.get(self) ?? 0.0
        }
        set(newValue) {
            MKMultiPolygon.lineWidth.set(self, value: newValue)
        }
    }

    public var strokeColor: UIColor {
        get {
            return MKMultiPolygon.strokeColor.get(self) ?? UIColor.black
        }
        set(newValue) {
            MKMultiPolygon.strokeColor.set(self, value: newValue)
        }
    }

    public var fillColor: UIColor {
        get {
            return MKMultiPolygon.fillColor.get(self) ?? UIColor.black
        }
        set(newValue) {
            MKMultiPolygon.fillColor.set(self, value: newValue)
        }
    }

    public func contains(_ coordinate2D: CLLocationCoordinate2D) -> Bool {

        return self.polygons.filter { $0.contains(coordinate2D) }.isEmpty ? false : true
    }

    public func copy(with zone: NSZone? = nil) -> Any {

        let copy = MKMultiPolygon(self.polygons)
        copy.lineWidth = self.lineWidth
        copy.strokeColor = self.strokeColor
        copy.fillColor = self.fillColor
        copy.id = self.id

        return copy
    }
}
