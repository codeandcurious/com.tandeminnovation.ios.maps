/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import CoreLocation
import MapKit

public class MapManager<T>: NSObject {

    //MARK: - Properties
    public private(set) var mapHelper: MapHelper?
    public private(set) var mapConfig: MapConfig?
    public private(set) var poiAdapter: PoiAdapter<T>?
    public private(set) var behaviour: MarkerManagerBehaviour<T>?

    private var locationHelper: LocationHelper?
    private var isMapShowing: Bool = false
    private var mapManagerProtocol: MapManagerProtocol?

    // MARK: - Computed Properties
    public var mapContainer: UIView? {

        return mapHelper?.containerView
    }
    public var locationPermissions: CLAuthorizationStatus? {
        return self.locationHelper?.locationAuthorizationStatus
    }

    //MARK: - Inits
    public init(mapHelper: MapHelper, mapConfig: MapConfig, poiAdapter: PoiAdapter<T>, behaviour: MarkerManagerBehaviour<T>, mapManagerProtocol: MapManagerProtocol) {

        self.mapHelper = mapHelper
        self.mapConfig = mapConfig
        self.poiAdapter = poiAdapter
        self.behaviour = behaviour
        self.mapManagerProtocol = mapManagerProtocol
        self.locationHelper = LocationHelper()

        self.mapHelper?.mapManagerProtocol = self.mapManagerProtocol
        super.init()
    }

    public func viewDidLoad() {

        setupCenterOnUser() // needs mapConfig, mapHelper
        setUserLocation() // needs mapConfig, locationHelper, mapHelper, mapDelegate
        mapManagerProtocol?.mapWillAppear()
        mapHelper?.locationChangedProtocol = self
    }

    public func viewDidUnload() {

        self.mapHelper = nil
        self.mapConfig = nil
        self.poiAdapter = nil
        self.behaviour = nil
        self.mapManagerProtocol = nil
        self.locationHelper = nil
    }

    fileprivate func setupCenterOnUser() {

        if mapConfig?.centerOnMe ?? false {

            setCenterOnMe()
        } else {

            if let initialCoordinates = mapConfig?.initialCoordinates {

                setCenter(on: initialCoordinates)
            }
        }
    }

    /// Set the center on me , note that it just centers on me if zoomOutToSeePois is false.
    /// If **zoomOutToSeePois == false** then the behaviour need to take "controll" of the zoom action
    /// behaviour can be the default or custom
    public func setCenterOnMe() {

        if !(mapConfig?.zoomOutPois ?? false) {

            mapHelper?.mapView.userTrackingMode = .follow
        }
    }

    public func setCenter(on centerPoint: CLLocationCoordinate2D) {

        mapHelper?.mapView.centerCoordinate = centerPoint
    }

    public func setPointLocationDelegate(to delegate: PointLocationListenerProtocol) {

        mapHelper?.setPointLocationDelegate(to: delegate)
    }

    func setUserLocation() {

        if mapConfig?.updateLocationRate == .never && !isMapShowing {

            showMap()
        } else {

            locationHelper?.setup(with: kCLLocationAccuracyBest)
            mapHelper?.showUserLocation = true
            mapHelper?.addMapToParentView()
        }
    }

    //MARK: - Map modifiers
    public func showMap() {

        mapManagerProtocol?.mapDidAppear()
        isMapShowing = true
    }

    /// Build and set MKPointAnnotations from a poiModelArray<T>
    ///   - Parameters:
    ///   - poiModelArray: The poiModel Array
    ///   - unwrapPinBlock: Closure to unwrap the poiModel to MKPointAnnotation
    @discardableResult public func setMapPois(_ poiModelArray: [T]?, _ closure: (T) -> MKPointAnnotation) -> [MapPoi<T>]? {

        if let poiModelArray: [T] = poiModelArray {

            poiAdapter = PoiAdapter<T>()
            poiAdapter?.setPois(poiModelArray: poiModelArray, closure: closure)
            if let mapPois = poiAdapter?.mapPois {

                behaviour?.populateMap(poiModelArray: mapPois)

                return mapPois
            }
        }

        return nil
    }

    /// Build and add MKPointAnnotations from a poiModelArray<T>
    ///   - Parameters:
    ///   - poiModelArray: The poiModel Array
    ///   - unwrapPinBlock: Closure to unwrap the poiModel to MKPointAnnotation
    @discardableResult public func addMapPois(_ poiModelArray: [T]?, _ closure: (T) -> MKPointAnnotation) -> [MapPoi<T>]? {

        if let poiModelArray: [T] = poiModelArray {

            if poiAdapter == nil { //if nil creates a new adapter

                poiAdapter = PoiAdapter<T>()
            }
            poiAdapter?.addPois(poiModelArray: poiModelArray, closure: closure)
            if let mapPois = poiAdapter?.mapPois {

                behaviour?.populateMap(poiModelArray: mapPois)

                return mapPois
            }
        }

        return nil
    }
    
    /// Removes from the current MapPoi array the given array and from the Map annotations
    /// - Parameter mapPois: mapPois varArgs
    public func removeMapPois(_ mapPois: MapPoi<T>...) {

        self.removeMKAnnotations(mapPois.compactMap { $0.mkPointAnnotation })
        poiAdapter?.removeMapPois(mapPois)
    }
    
    /// Removes from the current MapPoi array the given array and from the Map annotations
    /// - Parameter mapPois: mapPois array
    public func removeMapPois(_ mapPois: [MapPoi<T>]) {

        self.removeMKAnnotations(mapPois.compactMap { $0.mkPointAnnotation })
        poiAdapter?.removeMapPois(mapPois)
    }

    /// Remove all MKAnnotations in mapView
    public func removeAllMKAnnotations() {

        if let annotations: [MKAnnotation] = mapHelper?.mapView.annotations {

            mapHelper?.mapView.removeAnnotations(annotations)
        }
        poiAdapter?.removeAllMapPois()
    }

    /// Remove parameter annotations in mapView
    /// - Parameter annotations: annotations: [MKAnnotation]
    public func removeMKAnnotations(_ annotations: MKAnnotation...) {

        mapHelper?.mapView.removeAnnotations(annotations)
    }

    /// Remove parameter annotations in mapView
    /// - Parameter annotations: annotations: [MKAnnotation]
    public func removeMKAnnotations(_ annotations: [MKAnnotation]) {

        mapHelper?.mapView.removeAnnotations(annotations)
    }

    /// addAutomobileRoute
    /// - Parameters:
    ///   - sourceLocation: MapPointAnnotation
    ///   - destinationLocation: MapPointAnnotation
    public func addAutomobileRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation) {
        self.addRoute(sourceLocation, destinationLocation, MKDirectionsTransportType.automobile)
    }

    /// addWalkingRoute
    /// - Parameters:
    ///   - sourceLocation: MapPointAnnotation
    ///   - destinationLocation: MapPointAnnotation
    public func addWalkingRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation) {
        self.addRoute(sourceLocation, destinationLocation, MKDirectionsTransportType.walking)
    }

    /// addRoute
    /// - Parameters:
    ///   - sourceLocation: MapPointAnnotation
    ///   - destinationLocation: MapPointAnnotation
    public func addRoute(_ sourceLocation: MapPointAnnotation, _ destinationLocation: MapPointAnnotation, _ transportionType: MKDirectionsTransportType) {
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation.coordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation.coordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        self.mapHelper?.mapView.showAnnotations([sourceLocation, destinationLocation], animated: true)

        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = transportionType

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) -> Void in

            guard let response = response else {

                if let error = error {

                    print("Error: \(error)")
                }
                return
            }

            let route = response.routes[0]
            self.mapHelper?.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapHelper?.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }

    /// Variadic func to add any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
    /// - Parameter mkOverlays: MKOverlay...
    public func addMkOverlays(_ mkOverlays: MKOverlay...) {

        self.mapHelper?.mapView.addOverlays(mkOverlays)
    }

    /// Variadic func to add any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
    /// Needed because swift doesn't convert variadic func to array as parameters
    /// - Parameter mkOverlays: [MKOverlay]
    public func addMkOverlays(_ mkOverlays: [MKOverlay]) {

        self.mapHelper?.mapView.addOverlays(mkOverlays)
    }

    /// Remove all MkOverlays in the mkMapView
    public func removeAllMkOverlays() {
        if let mkOverlays: [MKOverlay] = mapHelper?.mapView.overlays {

            self.mapHelper?.mapView.removeOverlays(mkOverlays)
        }
    }

    /// Variadic func to remove any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
    /// - Parameter mkOverlays: MKOverlay...
    public func removeMkOverlays(_ mkOverlays: MKOverlay...) {

        self.mapHelper?.mapView.removeOverlays(mkOverlays)
    }

    /// Variadic func to remove any subClass of MKOverlay ex: MKPolygon, MKCircle, MKMultiPolygon, MKMultiPolyline, MKPolyline
    /// Needed because swift doesn't convert variadic func to array as parameters
    /// - Parameter mkOverlays: [MKOverlay]
    public func removeMkOverlays(_ mkOverlays: [MKOverlay]) {

        self.mapHelper?.mapView.removeOverlays(mkOverlays)
    }
}

extension MapManager: LocationChangedProtocol {

    public func onLocationsUpdated(_ locations: [CLLocation]) {
        if !isMapShowing {

            showMap()
        }
    }

    public func onLocationError(error: Error) {
    }
}
