/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit

public class MapPointAnnotation: MKPointAnnotation {

	public var selectedImage: UIImage? = nil
	public var unselectedImage: UIImage? = nil
	public var activeImage: UIImage? = nil
	public var inactiveImage: UIImage? = nil
	public var visitedImage: UIImage? = nil
	public var isSelected: Bool = false
	public var isActive: Bool = true
	public var isVisited: Bool = false
	public var pinAnnotationView: MKPinAnnotationView? = nil
	public var poiModel: MapPoiModel? = nil

	/**
     state map:
     
     - SELECTED | T | F |(T)| F |
     - ACTIVE   |(T)| T | T |(T)|
     - VISITED  | T |(T)| F | F |
     
     To work you need to set all images
     */
	public var pinImage: UIImage? {
		var result: UIImage? = selectedImage

		if (self.isSelected && self.isActive && self.isVisited) {

			result = self.activeImage
		}
		if (!self.isSelected && self.isActive && self.isVisited) {

			result = self.visitedImage
		}
		if (self.isSelected && self.isActive && !self.isVisited) {

			result = self.selectedImage
		}
		if (!self.isSelected && self.isActive && !self.isVisited) {

			result = self.activeImage
		}

		return result
	}
}
