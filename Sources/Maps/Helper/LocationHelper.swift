/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import CoreLocation

public class LocationHelper: NSObject {

    public static let shared = LocationHelper()
    public var locationManager: CLLocationManager
    public private(set) var currentLocation: CLLocation?
    public private(set) var currentLocations: [CLLocation]?
    public private(set) var lastKnowLocation: CLLocation?
    public var locationChangedProtocol: LocationChangedProtocol?
    public var locationAuthorizationStatus: CLAuthorizationStatus {

        return CLLocationManager.authorizationStatus()
    }

    public var hasLocationPermissions: Bool {

        switch locationAuthorizationStatus {

        case .notDetermined, .restricted, .denied:
            return false

        case .authorizedAlways, .authorizedWhenInUse:
            return true

        @unknown default:
            return false
        }
    }

    override init() {
        self.locationManager = CLLocationManager()
        self.lastKnowLocation = self.locationManager.location

        super.init()
    }

    /// kCLLocationAccuracyBestForNavigation: CLLocationAccuracy
    /// kCLLocationAccuracyBest: CLLocationAccuracy
    /// kCLLocationAccuracyNearestTenMeters: CLLocationAccuracy
    /// kCLLocationAccuracyHundredMeters: CLLocationAccuracy
    /// kCLLocationAccuracyKilometer: CLLocationAccuracy
    /// kCLLocationAccuracyThreeKilometers: CLLocationAccuracy
    /// - Parameter accuracy: ex: kCLLocationAccuracyBestForNavigation
    public func setup(with accuracy: CLLocationAccuracy) {
        self.setLocationHelperDelegate(accuracy: accuracy)
    }
    
    public func setupAndStart(with accuracy: CLLocationAccuracy) {
        self.setLocationHelperDelegate(accuracy: accuracy)
        self.startUpdatingLocation()
    }

    private func setLocationHelperDelegate(accuracy: CLLocationAccuracy) {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = accuracy
        self.locationManager.requestWhenInUseAuthorization()
    }

    // Start updating
    public func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {

            self.locationManager.startUpdatingLocation()
        }
    }

    public func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
    }
}

//MARK: - CLLocationManagerDelegate
extension LocationHelper: CLLocationManagerDelegate {

    // Tells the delegate that a new location value is available.
    public func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {

        self.currentLocation = newLocation
        self.locationChangedProtocol?.onLocationsUpdated([newLocation])
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        self.currentLocation = locations.first
        self.currentLocations = locations
        self.locationChangedProtocol?.onLocationsUpdated(locations)
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationChangedProtocol?.onLocationError(error: error)
    }

}
