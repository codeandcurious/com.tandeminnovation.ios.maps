/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit

public class MapBoundsHelper: NSObject {

	/**
     Calculates the NE and SW coords of a given mapView.
     - returns: Returns a array of CLLocationCoordinate2D with the fist [0] = neCoord and [1] = swCoord .
     */
	private func getFrameBoundsCoordinates(from mapView: MKMapView) -> [CLLocationCoordinate2D] {
		//Calculate the corners of the map so we get the points
		let nePoint: CGPoint = CGPoint(x: mapView.bounds.origin.x + mapView.bounds.size.width, y: mapView.bounds.origin.y)
		let swPoint: CGPoint = CGPoint(x: (mapView.bounds.origin.x), y: (mapView.bounds.origin.y + mapView.bounds.size.height))
		//Transform those point into lat,lng values
		let neCoord: CLLocationCoordinate2D = mapView.convert(nePoint, toCoordinateFrom: mapView)
		let swCoord: CLLocationCoordinate2D = mapView.convert(swPoint, toCoordinateFrom: mapView)

		return [neCoord, swCoord]
	}

	/**
     Calculates the NE NW SW SE coords of a given mapView.
     - returns: Returns a array of CLLocationCoordinate2D with: [0] = neCoord ; [1] = nwCoord ; [2] = swCoord ; [3] = seCoord .
     */
	public func getFullFrameBoundsCoordinates(from mapView: MKMapView) -> [CLLocationCoordinate2D] {
		let boundsCoordinates = self.getFrameBoundsCoordinates(from: mapView)
		let nE: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: boundsCoordinates[0].latitude, longitude: boundsCoordinates[0].longitude)
		let nW: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: boundsCoordinates[0].latitude, longitude: boundsCoordinates[1].longitude)
		let sW: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: boundsCoordinates[1].latitude, longitude: boundsCoordinates[1].longitude)
		let sE: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: boundsCoordinates[1].latitude, longitude: boundsCoordinates[0].longitude)

		return [nE, nW, sW, sE]
	}

	/**
     Calculates the center of a given mapView.
     - returns: CLLocationCoordinate2D with the center of the given mapView
     */
	public func getCenterPoint(mapView: MKMapView) -> CLLocationCoordinate2D {
		let boundsCoordinates = self.getFullFrameBoundsCoordinates(from: mapView)
		let nE: CLLocationCoordinate2D = boundsCoordinates[0]
		let nW: CLLocationCoordinate2D = boundsCoordinates[1]
		let sW: CLLocationCoordinate2D = boundsCoordinates[2]
		let sE: CLLocationCoordinate2D = boundsCoordinates[3]

		return CLLocationCoordinate2D(latitude: (nE.latitude + nW.latitude) / 2, longitude: (sW.longitude + sE.longitude) / 2)
	}
}
