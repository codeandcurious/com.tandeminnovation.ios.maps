/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit
import UIKit

public class MapHelper: NSObject {

    public private(set) var mapView: MapView
    private var mapConfig: MapConfig
    private var parentView: UIView?
    public private(set) var currentLocation: CLLocation?
    public var showUserLocation: Bool = false {
        didSet {

            self.mapView.showsUserLocation = self.showUserLocation
            self.mapView.userLocation.title = nil
        }
    }
    private var pointLocationListener: PointLocationListenerProtocol?
    public var containerView: UIView? {

        return self.parentView
    }
    public var locationChangedProtocol: LocationChangedProtocol?
    public var mapViewProtocol: MapViewProtocol?
    public var mapManagerProtocol: MapManagerProtocol?
    public var mapType: MKMapType {
        set {
            self.mapView.mapType = newValue
        }
        get {
            return self.mapView.mapType
        }
    }

    public convenience init(view: UIView, mapconfig: MapConfig, showUserLocation: Bool) {
        self.init(view: view, mapconfig: mapconfig, mapViewProtocol: nil, showUserLocation: showUserLocation)
    }

    public init(view: UIView, mapconfig: MapConfig, mapViewProtocol: MapViewProtocol?, showUserLocation: Bool) {
        self.parentView = view
        self.mapView = MapView(frame: view.frame)
        self.mapConfig = mapconfig
        super.init()

        self.addMapToParentView()
        self.mapViewProtocol = mapViewProtocol
        self.mapView.mapViewProtocol = mapViewProtocol
        self.mapView.delegate = self
        self.showUserLocation = showUserLocation
    }

    //MARK: map and parent view managing methods
    public func addMapToParentView() {
        self.parentView?.addSubview(self.mapView)

        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.parentView?.translatesAutoresizingMaskIntoConstraints = false

        let top = NSLayoutConstraint(item: self.mapView, attribute: .top, relatedBy: .equal, toItem: self.parentView!, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: self.mapView, attribute: .bottom, relatedBy: .equal, toItem: self.parentView, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: self.mapView, attribute: .leading, relatedBy: .equal, toItem: self.parentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: self.mapView, attribute: .trailing, relatedBy: .equal, toItem: self.parentView, attribute: .trailing, multiplier: 1, constant: 0)

        NSLayoutConstraint.activate([top, bottom, leading, trailing])
    }

    //MARK: Location call methods
    public func setPointLocationDelegate(to delegate: PointLocationListenerProtocol) {
        self.pointLocationListener = delegate
    }

    //MARK: - Map Region Update

    /// Centers the map in the given location with the provided radius
    public func centerMap(on location: CLLocation, regionRadius: Double) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
        //        zoomMap(byFactor: 0.05)
    }

    public func centerMapOnUser(regionRadius: Double) {

        let coordinate = self.mapView.userLocation.coordinate
        let lastKnowLocation = LocationHelper.shared.lastKnowLocation?.coordinate ?? CLLocationCoordinate2D()
        let userLocation = coordinate.latitude < -90 ? lastKnowLocation : coordinate

        let coordinateRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }

    public func zoomMap(byFactor delta: Double) {
        var region: MKCoordinateRegion = self.mapView.region
        var span: MKCoordinateSpan = mapView.region.span
        span.latitudeDelta *= delta
        span.longitudeDelta *= delta
        region.span = span
        self.mapView.setRegion(region, animated: true)
    }

    private func legacyRenderer(_ mapView: MKMapView, _ overlay: MKOverlay) -> MKOverlayRenderer {
        switch overlay {

        case let overlay as MKCircle:
            let mkCircleRenderer = MKCircleRenderer(overlay: overlay)
            mkCircleRenderer.lineWidth = overlay.lineWidth
            mkCircleRenderer.strokeColor = overlay.strokeColor
            mkCircleRenderer.fillColor = overlay.fillColor

            return mkCircleRenderer


        case let overlay as MKPolygon:
            let mkPolygonRenderer = MKPolygonRenderer(overlay: overlay)
            mkPolygonRenderer.lineWidth = overlay.lineWidth
            mkPolygonRenderer.strokeColor = overlay.strokeColor
            mkPolygonRenderer.fillColor = overlay.fillColor

            return mkPolygonRenderer

        case let overlay as MKPolyline:
            let mkPolylineRenderer = MKPolylineRenderer(overlay: overlay)
            mkPolylineRenderer.lineWidth = overlay.lineWidth
            mkPolylineRenderer.strokeColor = overlay.strokeColor
            mkPolylineRenderer.fillColor = overlay.fillColor

            return mkPolylineRenderer

        default:
            return MKTileOverlayRenderer(overlay: overlay)
        }
    }

    @available(iOS 13.0, *)
    private func renderer(_ mapView: MKMapView, _ overlay: MKOverlay) -> MKOverlayRenderer {
        switch overlay {

        case let overlay as MKCircle:
            let mkCircleRenderer = MKCircleRenderer(overlay: overlay)
            mkCircleRenderer.lineWidth = overlay.lineWidth
            mkCircleRenderer.strokeColor = overlay.strokeColor
            mkCircleRenderer.fillColor = overlay.fillColor

            return mkCircleRenderer

        case let overlay as MKMultiPolygon:
            let mkMultiPolygonRenderer = MKMultiPolygonRenderer(overlay: overlay)
            mkMultiPolygonRenderer.lineWidth = overlay.lineWidth
            mkMultiPolygonRenderer.strokeColor = overlay.strokeColor
            mkMultiPolygonRenderer.fillColor = overlay.fillColor

            return mkMultiPolygonRenderer

        case let overlay as MKMultiPolyline:
            let mkMultiPolylineRenderer = MKMultiPolylineRenderer(overlay: overlay)
            mkMultiPolylineRenderer.lineWidth = overlay.lineWidth
            mkMultiPolylineRenderer.strokeColor = overlay.strokeColor
            mkMultiPolylineRenderer.fillColor = overlay.fillColor

            return mkMultiPolylineRenderer

        case let overlay as MKPolygon:
            let mkPolygonRenderer = MKPolygonRenderer(overlay: overlay)
            mkPolygonRenderer.lineWidth = overlay.lineWidth
            mkPolygonRenderer.strokeColor = overlay.strokeColor
            mkPolygonRenderer.fillColor = overlay.fillColor

            return mkPolygonRenderer

        case let overlay as MKPolyline:
            let mkPolylineRenderer = MKPolylineRenderer(overlay: overlay)
            mkPolylineRenderer.lineWidth = overlay.lineWidth
            mkPolylineRenderer.strokeColor = overlay.strokeColor
            mkPolylineRenderer.fillColor = overlay.fillColor

            return mkPolylineRenderer

        default:
            return MKTileOverlayRenderer(overlay: overlay)
        }
    }
}

extension MapHelper: MKMapViewDelegate {

    //MARK: Location
    public func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {

        self.currentLocation = userLocation.location
        if let userLocation = userLocation.location {

            self.locationChangedProtocol?.onLocationsUpdated([userLocation])
        }
    }

    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapPointAnnotation {

            let mapAnnotation = annotation
            let mkAnnotationView: MKAnnotationView = MKAnnotationView()
            mkAnnotationView.displayPriority = .required

            if mapConfig.isClusteringActive {

                mkAnnotationView.cluster?.displayPriority = .required
                if let clusterIdentifier: String = mapAnnotation.poiModel?.clusterIdentifier {

                    mkAnnotationView.clusteringIdentifier = clusterIdentifier
                } else {

                    //This will not cluster by mapPoiModel type, it will cluster all in the same cluster
                    mkAnnotationView.clusteringIdentifier = "defaultClusterIdentifier"
                }
            }

            if let pinImage = mapAnnotation.pinImage {

                mkAnnotationView.image = pinImage
                mkAnnotationView.centerOffset = CGPoint(x: 0, y: -((mkAnnotationView.image?.size.height)! / 2))
            }

            mkAnnotationView.canShowCallout = true
            if let mkPinAnnotationView = mapAnnotation.pinAnnotationView {

                if let rightCalloutAccessoryView = mkPinAnnotationView.rightCalloutAccessoryView {

                    mkAnnotationView.rightCalloutAccessoryView = rightCalloutAccessoryView
                }
                if let leftCalloutAccessoryView = mkPinAnnotationView.leftCalloutAccessoryView {

                    mkAnnotationView.leftCalloutAccessoryView = leftCalloutAccessoryView
                }

                if let detailCalloutAccessoryView = mkPinAnnotationView.detailCalloutAccessoryView {

                    mkAnnotationView.detailCalloutAccessoryView = detailCalloutAccessoryView
                }
            }

            return mkAnnotationView
        } else if let cluster = annotation as? MKClusterAnnotation {

            let markerAnnotationView = MKMarkerAnnotationView()
            markerAnnotationView.glyphText = String(cluster.memberAnnotations.count)
            markerAnnotationView.markerTintColor = mapConfig.clusterTintColor
            markerAnnotationView.canShowCallout = false
            markerAnnotationView.displayPriority = .required
            markerAnnotationView.subtitleVisibility = .hidden

            return markerAnnotationView
        }

        return nil
    }

    public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

        if let viewAnnotation = view.annotation as? MapPointAnnotation {

            self.mapViewProtocol?.didSelectMKAnnotationView(mapView, mapPointAnnotation: viewAnnotation, mkAnnotationView: view)
            viewAnnotation.isSelected = true

            if let pinImage = viewAnnotation.pinImage {

                view.image = pinImage
            }

        } else {
            self.mapViewProtocol?.didSelectMKAnnotationView(mapView, mapPointAnnotation: nil, mkAnnotationView: view)
        }
    }

    public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {

        if let viewAnnotation = view.annotation as? MapPointAnnotation {

            self.mapViewProtocol?.didDeselectMKAnnotationView(mapView, mapPointAnnotation: viewAnnotation, mkAnnotationView: view)
            viewAnnotation.isSelected = false

            if let pinImage = viewAnnotation.pinImage {

                view.image = pinImage
            }
        } else {
            self.mapViewProtocol?.didDeselectMKAnnotationView(mapView, mapPointAnnotation: nil, mkAnnotationView: view)
        }
    }

    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        if #available(iOS 13.0, *) {

            return renderer(mapView, overlay)
        } else {

            return legacyRenderer(mapView, overlay)
        }
    }

    public func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {

        mapManagerProtocol?.mapWillMove()
    }

    public func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if let point = pointLocationListener?.willSendPoint(), let view = pointLocationListener?.willSendView() {
            let mapCoords = mapView.convert(point, toCoordinateFrom: mapView)
            self.pointLocationListener?.didReceive(mapCoordinates: mapCoords, screenCoordinates: point, forView: view)
        }

        mapManagerProtocol?.mapDidMove()
    }
}
