/*
 MIT License
 
 Copyright (c) 2021 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import MapKit
import UIKit

public class MapView: MKMapView {

    public var mapViewProtocol: MapViewProtocol?

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        if let touch = touches.first {

            if touch.tapCount == 1 {

                let touchLocation: CGPoint = touch.location(in: self)
                let touchCoordinate: CLLocationCoordinate2D = self.convert(touchLocation, toCoordinateFrom: self)

//                let sortedAnotations = self.annotations.sorted { (lhs, rhs) -> Bool in
//
//                    lhs.coordinate.distance(from: touchCoordinate) <= rhs.coordinate.distance(from: touchCoordinate)
//                }
//                var asdasd = sortedAnotations.first
//                if asdasd?.coordinate.distance(from: touchCoordinate) ?? Double.greatestFiniteMagnitude >= 2000.0 {

                var mkCircleList: [MKCircle] = self.overlays.compactMap { $0 as? MKCircle }
                mkCircleList = mkCircleList.filter { $0.contains(touchCoordinate) }
                if !mkCircleList.isEmpty {

                    self.mapViewProtocol?.didTapMKCircles(self, mkCircleList)
                }

                if #available(iOS 13.0, *) {

                    var mkMultiPolygonList: [MKMultiPolygon] = self.overlays.compactMap { $0 as? MKMultiPolygon }
                    mkMultiPolygonList = mkMultiPolygonList.filter { $0.contains(touchCoordinate) }
                    if !mkMultiPolygonList.isEmpty {

                        self.mapViewProtocol?.didTapMKMultiPolygons(self, mkMultiPolygonList)
                    }
                }

                var mkPolygonList: [MKPolygon] = self.overlays.compactMap { $0 as? MKPolygon }
                mkPolygonList = mkPolygonList.filter { $0.contains(touchCoordinate) }
                if !mkPolygonList.isEmpty {

                    self.mapViewProtocol?.didTapMKPolygons(self, mkPolygonList)
                }

                if #available(iOS 13.0, *) {

                    var mkMultiPolylineList: [MKMultiPolyline] = self.overlays.compactMap { $0 as? MKMultiPolyline }
                    mkMultiPolylineList = mkMultiPolylineList.filter { $0.contains(touchCoordinate) }
                    if !mkMultiPolylineList.isEmpty {

                        self.mapViewProtocol?.didTapMKMultiPolylines(self, mkMultiPolylineList)
                    }
                }

                var mkPolylineList: [MKPolyline] = self.overlays.compactMap { $0 as? MKPolyline }
                mkPolylineList = mkPolylineList.filter { $0.contains(touchCoordinate) }
                if !mkPolylineList.isEmpty {

                    self.mapViewProtocol?.didTapMKPolylines(self, mkPolylineList)
                }

                //TODO
                //var mkTileOverlayList: [MKTileOverlay] = self.overlays.compactMap { $0 as? MKTileOverlay }
                //mkTileOverlayList = mkTileOverlayList.filter { $0.contains(locationCoordinate) }
//                }

                self.mapViewProtocol?.didTapMap(self, touchCoordinate)
            }
        }

        super.touchesEnded(touches, with: event)
    }
}
